function delay(func) {
    setTimeout(func, 500);
}

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById("dialog-one-show-button").addEventListener('click', function () {
        delay(function () {
            document.getElementById("dialog-one").style.display = 'block';
        });
    });

    document.getElementById("dialog-two-hide-button").addEventListener('click', function () {
        delay(function () {
            document.getElementById("dialog-two").style.display = 'none';
        });
    });

    document.getElementById("dialog-three-create-button").addEventListener('click', function () {
        delay(function () {
            var el = "<div id='dialog-three'>dialog 3 contents</div>";
            document.getElementById("dialog-three-container").insertAdjacentHTML("beforeend", el);
        });
    });

    document.getElementById("dialog-four-remove-button").addEventListener('click', function () {
        delay(function () {
            var element = document.getElementById("dialog-four");
            element.parentNode.removeChild(element);
        });
    });

    document.getElementById("dialog-five-select-button").addEventListener('click', function () {
        delay(function () {
            document.getElementById("dialog-five").click();
        });
    });

    document.getElementById("dialog-six-select-button").addEventListener('click', function () {
        delay(function () {
            document.getElementById("dialog-six").click();
        });
    });

    document.getElementById("dialog-seven-enable-button").addEventListener('click', function () {
        delay(function () {
            document.getElementById("dialog-seven").removeAttribute('disabled');
        });
    });

    document.getElementById("dialog-eight-disable-button").addEventListener('click', function () {
        delay(function () {
            document.getElementById("dialog-eight").setAttribute('disabled', 'disabled');
        });
    });

    document.getElementById("dialog-nine-addclass-button").addEventListener('click', function () {
        delay(function () {
            document.getElementById("dialog-nine").classList.add("awesome");
        });
    });

    document.getElementById("dialog-ten-removeclass-button").addEventListener('click', function () {
        delay(function () {
            document.getElementById("dialog-ten").classList.remove("awesome");
        });
    });

    document.getElementById("dialog-eleven-addtext-button").addEventListener('click', function () {
        delay(function () {
            document.getElementById("dialog-eleven").innerText = "Dialog eleven";
        });
    });

    document.getElementById("dialog-twelve-addattribute-button").addEventListener('click', function () {
        delay(function () {
            document.getElementById("dialog-twelve").setAttribute('data-test', 'value');
        });
    });


    document.getElementById("dialog-thirteen-addattributeandclass-button").addEventListener('click', function () {
        delay(function () {
            document.getElementById("dialog-thirteen").innerText = 'Dialog thirteen';
            delay(function () {
                document.getElementById("dialog-thirteen").classList.add('awesome');
            });
        });
    });
});
