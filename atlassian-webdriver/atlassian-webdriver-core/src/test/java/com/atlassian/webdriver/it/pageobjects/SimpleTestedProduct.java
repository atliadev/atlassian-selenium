package com.atlassian.webdriver.it.pageobjects;

import com.atlassian.pageobjects.*;
import com.atlassian.pageobjects.binder.InjectPageBinder;
import com.atlassian.pageobjects.binder.LoggerModule;
import com.atlassian.pageobjects.binder.StandardModule;
import com.atlassian.webdriver.WebDriverModule;
import com.atlassian.webdriver.pageobjects.DefaultWebDriverTester;
import com.atlassian.webdriver.pageobjects.WebDriverTester;

import static java.util.Objects.requireNonNull;


@Defaults(instanceId = "testapp", contextPath = "/", httpPort = 5990)
public class SimpleTestedProduct implements TestedProduct<WebDriverTester>
{
    private final PageBinder pageBinder;
    private final WebDriverTester  webDriverTester;
    private final ProductInstance productInstance;

    public SimpleTestedProduct(TestedProductFactory.TesterFactory<WebDriverTester> testerFactory, ProductInstance productInstance)
    {
        this.productInstance = requireNonNull(productInstance, "productInstance can't be null");

        this.webDriverTester =  new DefaultWebDriverTester();
        this.pageBinder = new InjectPageBinder(productInstance, webDriverTester,
                new StandardModule(this),
                new WebDriverModule(this),
                new LoggerModule(SimpleTestedProduct.class));
    }

    public <P extends Page> P visit(Class<P> pageClass, Object... args)
    {
        return pageBinder.navigateToAndBind(pageClass, args);
    }

    public PageBinder getPageBinder()
    {
        return pageBinder;
    }

    public ProductInstance getProductInstance()
    {
        return productInstance;
    }

    public WebDriverTester getTester()
    {
        return webDriverTester;
    }
}
