package com.atlassian.webdriver.it.tests;

import com.atlassian.webdriver.it.AbstractSimpleServerTest;
import com.atlassian.webdriver.it.pageobjects.page.ElementDisplayednessPage;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class TestElementDisplayedness extends AbstractSimpleServerTest {
    private WebDriver driver;

    @Before
    public void init() {
        product.visit(ElementDisplayednessPage.class);
        driver = product.getTester().getDriver();
    }

    @Test
    public void testVanillaElementIsVisible() {
        WebElement el = driver.findElement(By.cssSelector("#plain-visible input"));
        assertThat(el.isDisplayed(), is(true));
    }

    @Test
    public void testElementWithZeroOpacityIsInvisible() {
        WebElement el = driver.findElement(By.cssSelector("#zero-opacity input"));
        assertThat(el.isDisplayed(), is(false));
    }

    @Test
    public void testElementOutsideOverflowBoundaryIsInvisible() {
        WebElement el = driver.findElement(By.cssSelector("#pseudo-checkbox-1 input"));
        assertThat(el.isDisplayed(), is(false));
    }

    @Test
    public void testElementWithNegativeZIndexIsInvisible() {
        WebElement el = driver.findElement(By.cssSelector("#negative-z-index input"));
        assertThat(el.isDisplayed(), is(false));
    }

    @Test
    public void testElementWithClipTrickIsVisible() {
        WebElement el = driver.findElement(By.cssSelector("#pseudo-checkbox-2 input"));
        assertThat(el.isDisplayed(), is(true));
    }

    @Test
    public void testElementWithAggressiveClipTrickIsVisible() {
        WebElement el = driver.findElement(By.cssSelector("#pseudo-checkbox-3 input"));
        assertThat(el.isDisplayed(), is(true));
    }

}
