package com.atlassian.webdriver.pageobjects;

import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.pageobjects.browser.BrowserAware;
import com.atlassian.webdriver.LifecycleAwareWebDriverGrid;
import com.atlassian.webdriver.WebDriverContext;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation that uses WebDriver to drive the browser.
 */
public class DefaultWebDriverTester implements WebDriverTester, BrowserAware
{
    private static final Logger log = LoggerFactory.getLogger(DefaultWebDriverTester.class);

    private final WebDriverContext context;

    public DefaultWebDriverTester()
    {
        context = LifecycleAwareWebDriverGrid.getDriverContext();
    }

    public WebDriver getDriver()
    {
        return context.getDriver();
    }

    public void gotoUrl(String url)
    {
        log.debug("Navigating to URL: " + url);
        context.getDriver().get(url);
    }

    @Override
    public Browser getBrowser()
    {
        return context.getBrowser();
    }
}
