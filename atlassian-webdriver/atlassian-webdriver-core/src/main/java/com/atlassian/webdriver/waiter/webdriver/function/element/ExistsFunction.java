package com.atlassian.webdriver.waiter.webdriver.function.element;

import com.atlassian.webdriver.waiter.webdriver.function.ConditionFunction;
import com.atlassian.webdriver.waiter.webdriver.retriever.WebElementRetriever;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;

/**
 * @since 2.1.0
 */
public class ExistsFunction implements ConditionFunction
{
    private final WebElementRetriever elementRetriever;

    public ExistsFunction(final WebElementRetriever elementRetriever)
    {
        this.elementRetriever = elementRetriever;
    }

    public Boolean apply(WebDriver from)
    {
        try
        {
            // Make any call on the element.
            elementRetriever.retrieveElement().isDisplayed();
            return true;
        }
        catch (NoSuchElementException | StaleElementReferenceException e)
        {
            return false;
        }
    }
}
