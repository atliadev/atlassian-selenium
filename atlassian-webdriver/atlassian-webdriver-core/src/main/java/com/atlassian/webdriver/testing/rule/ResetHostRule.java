package com.atlassian.webdriver.testing.rule;

import com.atlassian.pageobjects.MultiTenantTestedProduct;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class ResetHostRule implements TestRule {

    private final MultiTenantTestedProduct testedProduct;

    public ResetHostRule(final MultiTenantTestedProduct testedProduct) {
        this.testedProduct = testedProduct;
    }

    @Override
    public Statement apply(final Statement statement, final Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                testedProduct.resetToDefaultHost();
            }
        };
    }

}