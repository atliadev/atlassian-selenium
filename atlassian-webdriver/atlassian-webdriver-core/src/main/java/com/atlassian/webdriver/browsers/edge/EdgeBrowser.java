package com.atlassian.webdriver.browsers.edge;

import com.atlassian.browsers.BrowserConfig;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeDriverService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;

import static org.apache.commons.lang3.SystemUtils.IS_OS_WINDOWS;

/**
 *
 */
public final class EdgeBrowser
{
    private static final Logger log = LoggerFactory.getLogger(EdgeBrowser.class);

    public static final String EDGE_SERVICE_EXECUTABLE = "msedgedriver" + (IS_OS_WINDOWS ? ".exe" : "");

    private EdgeBrowser()
    {
        throw new AssertionError("Don't instantiate me");
    }

    public static EdgeDriver createEdgeDriver(@Nullable String browserPath, @Nullable BrowserConfig config)
    {
        if (noBrowserPath(browserPath) && noServicePath(config))
        {
            return createDefaultDriver();
        }
        else
        {
            final EdgeDriverService service = setBrowserExecutablePath(browserPath,
                    setServiceExecutablePath(config, new EdgeDriverService.Builder()))
                    .usingAnyFreePort()
                    .build();
            return new EdgeDriver(service);
        }
    }

    public static EdgeDriver createDefaultDriver()
    {
        return new EdgeDriver();
    }

    private static boolean noBrowserPath(@Nullable String browserPath)
    {
        return browserPath == null;
    }

    private static boolean hasServicePath(@Nullable BrowserConfig config)
        {
            return config != null && config.getProfilePath() != null;
        }

    private static boolean noServicePath(@Nullable BrowserConfig config)
    {
        return !hasServicePath(config);
    }

    private static EdgeDriverService.Builder setServiceExecutablePath(BrowserConfig browserConfig, EdgeDriverService.Builder builder)
    {
        if (hasServicePath(browserConfig))
        {
            File profilePath = new File(browserConfig.getProfilePath());
            File edgeDriverFile = new File(profilePath, EDGE_SERVICE_EXECUTABLE);
            if (edgeDriverFile.isFile())
            {
                builder.usingDriverExecutable(edgeDriverFile);
            }
        }
        return builder;
    }

    private static EdgeDriverService.Builder setBrowserExecutablePath(String browserPath, EdgeDriverService.Builder builder)
        {
            if (browserPath != null)
            {
                // can't do much here, IE driver knows better where to look for IE
                log.warn("Non-null browser path configured for IE: '{}', but IEDriver does not support custom browser paths", browserPath);
            }
            return builder;
        }

}
