package com.atlassian.pageobjects.elements.query.util;

import java.util.concurrent.Callable;

public interface NamedCallable<T> extends Callable<T>
{
    String Name();
}
