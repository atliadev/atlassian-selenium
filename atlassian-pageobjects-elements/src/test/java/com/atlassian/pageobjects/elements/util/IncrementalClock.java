package com.atlassian.pageobjects.elements.util;

import com.atlassian.pageobjects.elements.query.util.Clock;

public class IncrementalClock implements Clock
{
    private final long step ;
    private long time ;

    public IncrementalClock(long step) {
        this.step = step;
        //so first value returned is zero
        this.time = 0-step;
    }

    @Override
    public long currentTime()
    {
        return time += step;
    }
}
